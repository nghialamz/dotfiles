return {
    {
        "nvim-lualine/lualine.nvim",
        opts = {
            options = {
                icons_enabled = true,
                component_separators = "|",
                section_separators = "",
            },
        },
    },
    {
        "sainnhe/everforest",
        lazy = false,
        priority = 1000,
        config = function()
            -- Optionally configure and load the colorscheme
            -- directly inside the plugin declaration.
            vim.g.everforest_enable_italic = false
            vim.g.everforest_background = "hard"
            vim.g.everforest_better_performance = 1
            vim.cmd.colorscheme("everforest")
        end,
    },
    {
        "alljokecake/naysayer-theme.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            require('naysayer').setup({
                variant = 'main',
                dark_variant = 'main',
                bold_vert_split = false,
                dim_nc_background = false,
                disable_background = true,
                disable_float_background = true,
                disable_italics = true,
            })
            -- vim.cmd.colorscheme("naysayer")
        end,
    },
    {
        "slugbyte/lackluster.nvim",
        lazy = false,
        priority = 1000,
        init = function()
            -- vim.cmd.colorscheme("lackluster")
            -- vim.cmd.colorscheme("lackluster-hack")
            -- vim.cmd.colorscheme("lackluster-mint")
        end,
    },
    {
        'echasnovski/mini.nvim',
        version = false,
        init = function()
            -- vim.cmd.colorscheme("minicyan")
        end,
    },
    {
        "xero/miasma.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            --vim.cmd("colorscheme miasma")
        end,
    },
    {
        'blackbirdtheme/vim',
        lazy = false,
        priority = 1000,
        init = function()
            -- vim.cmd.colorscheme("blackbird")
        end,
    },
    {
        'jnurmine/Zenburn',
        lazy = false,
        priority = 1000,
        init = function()
            -- vim.cmd.colorscheme("zenburn")
        end,
    },
    {
        'TaurusOlson/darkburn.vim',
        lazy = false,
        priority = 1000,
        init = function()
            -- vim.cmd.colorscheme("darkburn")
        end,
    },
}
