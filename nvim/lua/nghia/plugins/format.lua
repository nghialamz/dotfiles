return {
    "nvimtools/none-ls.nvim",
    config = function()
        local null_ls = require("null-ls")
        null_ls.setup({
            sources = {
                -- Lua
                null_ls.builtins.formatting.stylua,

                -- Cpp
                null_ls.builtins.formatting.clang_format,
                null_ls.builtins.diagnostics.cppcheck,

                -- CMake
                null_ls.builtins.formatting.cmake_format,
                null_ls.builtins.diagnostics.cmake_lint,

                -- Python
                null_ls.builtins.formatting.yapf,
                null_ls.builtins.diagnostics.pylint,
            },
        })

        vim.keymap.set("n", "<leader>cf", vim.lsp.buf.format, {})
    end,
}
