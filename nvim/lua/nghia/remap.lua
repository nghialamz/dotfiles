vim.g.mapleader = " "

-- Buffer actions mappings
vim.keymap.set('n', '<leader>bs', ':w<CR>')

-- Window actions mappings
vim.keymap.set('n', '<leader>wv', ':vsplit<CR>')
vim.keymap.set('n', '<leader>ws', ':split<CR>')
vim.keymap.set('n', '<leader>wl', '<C-w><C-l>')
vim.keymap.set('n', '<leader>wh', '<C-w><C-h>')
vim.keymap.set('n', '<leader>wj', '<C-w><C-j>')
vim.keymap.set('n', '<leader>wk', '<C-w><C-k>')

-- Normal mode mappings
vim.keymap.set('n', 'H', '^')
vim.keymap.set('n', 'L', '$')
vim.keymap.set('v', 'H', '^')
vim.keymap.set('v', 'L', '$')

-- Insert mode mappings
vim.keymap.set('i', 'jk', '<esc>')
