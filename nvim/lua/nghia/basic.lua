vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false

-- Neovide config
if vim.g.neovide then
    vim.o.guifont = "Hurmit Nerd Font:h10"
    vim.opt.linespace = 0
    vim.g.neovide_position_animation_length = 0.01
    vim.g.neovide_scroll_animation_length = 0.03
    vim.g.neovide_cursor_animation_length = 0.05
end
