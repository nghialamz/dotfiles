local cfg = {
	default_prog = {
		"pwsh.exe",
		"-NoLogo",
	},

	font_size = 11.0,
}

return cfg
