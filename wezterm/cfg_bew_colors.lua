local col = {}

-- Bew colors
col.background = "#2b3339"
col.foreground = "#d3c6aa"

col.cursor_bg = "#d3c6aa"
col.cursor_fg = "#202020"
col.cursor_border = "#d3c6aa" -- same as cursor_bg

col.ansi = {
    "#2F2F2F", -- black
    "#e67e80", -- red
    "#a7c080", -- green
    "#dbbc7f", -- yellow
    "#7fbbb3", -- blue
    "#e69875", -- orange (magentas usually)
    "#83c092", -- cyan
    "#cccccc", -- white
}

col.brights = {
    "#555753", -- black
    "#e67e80", -- red
    "#a7c080", -- green
    "#dbbc7f", -- yellow
    "#7fbbb3", -- blue
    "#e69875", -- orange (magentas usually)
    "#83c092", -- cyan
    "#fafafa", -- white
}

col.tab_bar = {
    background = "#2b3339",
    new_tab = { bg_color = "#2b3339", fg_color = "#FCE8C3", intensity = "Bold" },
    new_tab_hover = { bg_color = "#2b3339", fg_color = "#FBB829", intensity = "Bold" },
    active_tab = { bg_color = "#2b3339", fg_color = "#FCE8C3" },
}

return col
