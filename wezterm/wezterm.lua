-- WezTerm configuration
---------------------------------------------------------------

local mytable = require("lib/mystdlib").mytable
local wezterm = require("wezterm")

wezterm.GLOBAL.tab_titles = wezterm.GLOBAL.tab_titles or {}

local function basename(s)
	return string.gsub(s, "(.*[/\\])(.*)", "%2")
end
local SOLID_LEFT_ARROW = utf8.char(0xe0ba)
local SOLID_LEFT_MOST = utf8.char(0x2588)
local SOLID_RIGHT_ARROW = utf8.char(0xe0bc)

local ADMIN_ICON = utf8.char(0xf49c)

local CMD_ICON = utf8.char(0xe62a)
local NU_ICON = utf8.char(0xe7a8)
local PS_ICON = utf8.char(0xe70f)
local ELV_ICON = utf8.char(0xfc6f)
local WSL_ICON = utf8.char(0xf83c)
local YORI_ICON = utf8.char(0xf1d4)
local NYA_ICON = utf8.char(0xf61a)

local VIM_ICON = utf8.char(0xe62b)
local PAGER_ICON = utf8.char(0xf718)
local FUZZY_ICON = utf8.char(0xf0b0)
local HOURGLASS_ICON = utf8.char(0xf252)
local SUNGLASS_ICON = utf8.char(0xf9df)

local PYTHON_ICON = utf8.char(0xf820)
local NODE_ICON = utf8.char(0xe74e)
local DENO_ICON = utf8.char(0xe628)
local LAMBDA_ICON = utf8.char(0xfb26)

local SUP_IDX = {
	"¹",
	"²",
	"³",
	"⁴",
	"⁵",
	"⁶",
	"⁷",
	"⁸",
	"⁹",
	"¹⁰",
	"¹¹",
	"¹²",
	"¹³",
	"¹⁴",
	"¹⁵",
	"¹⁶",
	"¹⁷",
	"¹⁸",
	"¹⁹",
	"²⁰",
}
local SUB_IDX = {
	"₁",
	"₂",
	"₃",
	"₄",
	"₅",
	"₆",
	"₇",
	"₈",
	"₉",
	"₁₀",
	"₁₁",
	"₁₂",
	"₁₃",
	"₁₄",
	"₁₅",
	"₁₆",
	"₁₇",
	"₁₈",
	"₁₉",
	"₂₀",
}

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
	local edge_background = "#2b3339"
	local background = "#4E4E4E"
	local foreground = "#1C1B19"
	local dim_foreground = "#2b3339"

	if tab.is_active then
		background = "#FBB829"
		foreground = "#1C1B19"
	elseif hover then
		background = "#FF8700"
		foreground = "#1C1B19"
	end

	local edge_foreground = background
	local process_name = tab.active_pane.foreground_process_name
	local pane_title = tab.active_pane.title
	local exec_name = basename(process_name):gsub("%.exe$", "")
	local tab_id = tostring(tab.tab_id)
	local user_title = wezterm.GLOBAL.tab_titles[tab_id] or nil
	local title_with_icon

	if user_title ~= nil and #user_title > 0 then
		pane_title = user_title
	end

	if exec_name == "nu" then
		title_with_icon = NU_ICON .. " NuShell"
	elseif exec_name == "elvish" then
		title_with_icon = ELV_ICON .. " Elvish"
	elseif exec_name == "wsl" or exec_name == "wslhost" then
		title_with_icon = WSL_ICON .. " WSL"
	elseif exec_name == "nyagos" then
		title_with_icon = NYA_ICON .. " " .. pane_title:gsub(".*: (.+) %- .+", "%1")
	elseif exec_name == "yori" then
		title_with_icon = YORI_ICON .. " " .. pane_title:gsub(" %- Yori", "")
	elseif exec_name == "nvim" then
		title_with_icon = VIM_ICON .. " " .. pane_title:gsub("^(%S+)%s+(%d+/%d+) %- nvim", " %2 %1")
	elseif exec_name == "bat" or exec_name == "less" or exec_name == "moar" then
		title_with_icon = PAGER_ICON .. " " .. exec_name:upper()
	elseif exec_name == "fzf" or exec_name == "hs" or exec_name == "peco" then
		title_with_icon = FUZZY_ICON .. " " .. exec_name:upper()
	elseif exec_name == "btm" or exec_name == "ntop" then
		title_with_icon = SUNGLASS_ICON .. " " .. exec_name:upper()
	elseif exec_name == "python" or exec_name == "hiss" then
		title_with_icon = PYTHON_ICON .. " " .. exec_name
	elseif exec_name == "node" then
		title_with_icon = NODE_ICON .. " " .. exec_name:upper()
	elseif exec_name == "deno" then
		title_with_icon = DENO_ICON .. " " .. exec_name:upper()
	elseif exec_name == "bb" or exec_name == "cmd-clj" or exec_name == "janet" or exec_name == "hy" then
		title_with_icon = LAMBDA_ICON .. " " .. exec_name:gsub("bb", "Babashka"):gsub("cmd%-clj", "Clojure")
	elseif exec_name == "pwsh" then
		title_with_icon = PS_ICON .. " " .. pane_title
	elseif exec_name == "cmd" then
		title_with_icon = CMD_ICON .. " " .. pane_title
	else
		title_with_icon = HOURGLASS_ICON .. " " .. exec_name
	end
	if pane_title:match("^Administrator: ") then
		title_with_icon = title_with_icon .. " " .. ADMIN_ICON
	end
	local left_arrow = SOLID_LEFT_ARROW
	if tab.tab_index == 0 then
		left_arrow = SOLID_LEFT_MOST
	end
	local id = SUB_IDX[tab.tab_index + 1]
	local pid = SUP_IDX[tab.active_pane.pane_index + 1]
	local title = " " .. wezterm.truncate_right(title_with_icon, max_width - 6) .. " "

	return {
		{ Attribute = { Intensity = "Bold" } },
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = left_arrow },
		{ Background = { Color = background } },
		{ Foreground = { Color = foreground } },
		{ Text = id },
		{ Text = title },
		{ Foreground = { Color = dim_foreground } },
		{ Text = pid },
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = SOLID_RIGHT_ARROW },
		{ Attribute = { Intensity = "Normal" } },
	}
end)

-- Misc
------------------------------------------

local cfg_misc = {
	window_close_confirmation = "NeverPrompt",
	check_for_updates = true,

	-- Use the powershell as default program
	-- default_prog = {"C:/Program Files/WindowsApps/Microsoft.PowerShell_7.2.4.0_x64__8wekyb3d8bbwe/pwsh.exe", "-NoLogo"},

	-- We can disable this if we want to avoid unexpected config breakage and
	-- unusable terminal
	automatically_reload_config = true,

	-- Make sure word selection stops on most punctuations.
	-- Note that dot (.) & slash (/) are allowed though for
	-- easy selection of paths.
	selection_word_boundary = " \t\n{}[]()\"'`,;:@",

	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = false,
	window_decorations = "RESIZE",

	-- Do not hold on exit by default.
	-- Because the default 'CloseOnCleanExit' can be annoying when exiting with
	-- Ctrl-D and the last command exited with non-zero: the shell will exit
	-- with non-zero and the terminal would hang until the window is closed manually.
	exit_behavior = "Close",

	-- Pad window to avoid the content to be too close to the border,
	-- so it's easier to see and select.
	window_padding = {
		left = 5,
		right = 3,
		top = 5,
		bottom = 2,
	},

	-- cf the original issue (mine): https://github.com/wez/wezterm/issues/478 solved for me but not for everyone..
	-- cf the addition of this flag: https://github.com/wez/wezterm/commit/336f209ede27dd801f989419155e475f677e8244
	-- OK BUT NO, disabled because it does some weird visual artifacts:
	--  * About cursor behaviors:
	--    When a ligature is a the end of the line & the nvim' window
	--    is a little bit larger than the text so that when the cursor comes
	--    closer to the window border (and on the ligature), the buffer does
	--    a side-scroll. Then the cursor does wonky stuff when moving w.r.t that
	--    end-of-line ligature.
	--
	--  * About some symbols display:
	--    The git above/below arrows on the right of my prompt.
	--
	-- experimental_shape_post_processing = true,

	tab_max_width = 40,
	tab_bar_at_bottom = false,
	-- intentionally both empty as we use the title bar function
	--tab_bar_style = {
	--active_tab_left = empty,
	--active_tab_right = empty,
	--inactive_tab_left = empty,
	--inactive_tab_right = empty,
	--inactive_tab_hover_left = empty,
	--inactive_tab_hover_right = empty,
	--},
}

-- Colors & Appearance
------------------------------------------

local cfg_colors = {
	colors = require("cfg_bew_colors"),
	color_scheme = col,
}

-- Font
------------------------------------------

local cfg_fonts = require("cfg_fonts")

-- Key/Mouse bindings
------------------------------------------

-- Key bindings
local cfg_key_bindings = require("cfg_keys")

-- Mouse bindings
local cfg_mouse_bindings = require("cfg_mouse")

wezterm.on("update-right-status", function(window, pane)
	-- Each element holds the text for a cell in a "powerline" style << fade
	local cells = {}

	-- Figure out the cwd and host of the current pane.
	-- This will pick up the hostname for the remote host if your
	-- shell is using OSC 7 on the remote host.
	local cwd_uri = pane:get_current_working_dir()
	if cwd_uri then
		cwd_uri = cwd_uri:sub(8)
		local slash = cwd_uri:find("/")
		local cwd = ""
		local hostname = ""
		if slash then
			hostname = cwd_uri:sub(1, slash - 1)
			-- Remove the domain name portion of the hostname
			local dot = hostname:find("[.]")
			if dot then
				hostname = hostname:sub(1, dot - 1)
			end
			-- and extract the cwd from the uri
			cwd = cwd_uri:sub(slash)

			table.insert(cells, cwd)
			table.insert(cells, hostname)
		end
	end

	-- I like my date/time in this style: "Wed Mar 3 08:14"
	local date = wezterm.strftime("%a %b %-d %H:%M")
	table.insert(cells, date)

	-- An entry for each battery (typically 0 or 1 battery)
	for _, b in ipairs(wezterm.battery_info()) do
		table.insert(cells, string.format("%.0f%%", b.state_of_charge * 100))
	end

	-- Color palette for the backgrounds of each cell
	local colors = {
		"#00af87",
		"#dbbc7f",
		"#7fbbb3",
		"#e69875",
		"#83c092",
	}

	-- Foreground color for the text across the fade
	local text_fg = "#ffffff"

	-- The elements to be formatted
	local elements = {}
	-- How many cells have been formatted
	local num_cells = 0

	-- Translate a cell into elements
	function push(text, is_last)
		local cell_no = num_cells + 1
		table.insert(elements, { Foreground = { Color = text_fg } })
		table.insert(elements, { Background = { Color = colors[cell_no] } })
		table.insert(elements, { Text = " " .. text .. " " })
		if not is_last then
			table.insert(elements, { Foreground = { Color = colors[cell_no + 1] } })
			--table.insert(elements, {Text=SOLID_LEFT_ARROW})
		end
		num_cells = num_cells + 1
	end

	while #cells > 0 do
		local cell = table.remove(cells, 1)
		push(cell, #cells == 0)
	end

	window:set_right_status(wezterm.format(elements))
end)

-- OS
------------------------------------------

local cfg_win32 = require("cfg_win32")

local cfg_os = wezterm.target_triple == "x86_64-pc-windows-msvc" and cfg_win32 or {}

-- Merge configs and return!
------------------------------------------
local config = mytable.merge_all(cfg_misc, cfg_colors, cfg_fonts, cfg_key_bindings, cfg_mouse_bindings, cfg_os)

return config
