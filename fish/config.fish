# A file by:

#  ___              _    _                 ___          _
# | _ \ __ _  _ _  | |_ | | ___  ___ ___  / __| ___  __| | ___  _ _
# |  _// _` || ' \ |  _|| |/ -_)(_-<(_-< | (__ / _ \/ _` |/ -_)| '_|
# |_|  \__,_||_||_| \__||_|\___|/__//__/  \___|\___/\__,_|\___||_|
#
# Nghia Lam (Pantless Coder)
# https://codeberg.org/pantlesscoder/dotfiles

#
# ~/config.fish

####################################################################
# Settings
####################################################################

if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_greeting
    echo Hello, Nghia.
    echo
    echo Current time is (set_color yellow; date +%T; set_color normal), lets have a nice day!!
    echo
    echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^O^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    echo
end

####################################################################
# Export
####################################################################
set TERM "xterm-256color"                      # Sets the terminal type
set EDITOR "emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
set VISUAL "emacsclient -c -a emacs"           # $VISUAL use Emacs in GUI mode

####################################################################
# Alias
####################################################################

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# vim and emacs
alias vm='nvim'
alias vim='nvim'
alias emacs="emacsclient -c -a 'emacs'"
alias doominstall="~/.emacs.d/bin/doom install"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"

# git
alias gpush="git push"
alias gpull="git pull"
alias lg="lazygit"

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first --icons' # my preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.='exa -a | egrep "^\."'

switch (uname)
  case Darwin
    source (dirname (status --current-filename))/config-osx.fish
  case Linux
    source (dirname (status --current-filename))/config-linux.fish
  case '*'
    source (dirname (status --current-filename))/config-windows.fish
end

if status is-interactive
    # Commands to run in interactive sessions can go here
end

starship init fish | source
