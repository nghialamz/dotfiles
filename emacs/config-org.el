;;; config-org.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Configure the Org mode of Emacs. Used mostly for writing documents.

;; Table of Content enable
;; ---

(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))
