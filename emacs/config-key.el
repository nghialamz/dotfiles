;;; config-key.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Remap keys and setup vim mode for Emacs

;; Evil package install
;; ---
(use-package evil
  :demand t
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  :config
  (evil-mode t))

(use-package evil-collection
  :after evil
  :config
  ;; (setq evil-collection-mode-list '(dashboard dired ibuffer))
  (evil-collection-init))

(use-package evil-escape
  :after evil
  :config
  (setq-default evil-escape-key-sequence "jk")
  (evil-escape-mode))

(use-package evil-tutor)
(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil))

;; which-key package install
;; ---
(use-package which-key
  :init
  (which-key-mode 1)
  :config
  (setq which-key-side-window-location 'bottom
		which-key-sort-order #'which-key-key-order-alpha
		which-key-sort-uppercase-first nil
		which-key-add-column-padding 1
		which-key-max-display-columns nil
		which-key-min-display-lines 6
		which-key-side-window-slot -10
		which-key-side-window-max-height 0.25
		which-key-idle-delay 0.8
		which-key-max-description-length 25
		which-key-allow-imprecise-window-fit t
		which-key-separator " -> " ))

;; General keybindings
;; ---
(use-package general
  :config
  (general-evil-setup)

  ;; Setup 'SPC' as the global leader key
  (general-create-definer nghia/leader-keys
    :states '(normal visual insert emacs)
    :keymaps 'override
    :prefix "SPC" ;; Set leader
    :global-prefix "C-SPC") ;; access leader in insert mode

  (general-define-key
    :states 'motion
    "H" 'evil-first-non-blank-of-visual-line
    "L" 'evil-end-of-line-or-visual-line)

  ;; My keymap
  
  (nghia/leader-keys
	"TAB" '(perspective-map :wk "Perspective") ;; Lists all the perspective keybindings
    "SPC" '(nghia/reload-init-file :wk "Reload Emacs Configs"))

  (nghia/leader-keys
    "b" '(:ignore t :wk "Buffers")
    "b b" '(switch-to-buffer :wk "Switch buffer")
    "b i" '(helm-mini :wk "Find buffer mini")
    "b f" '(helm-find-files :wk "Find files")
    "b k" '(kill-this-buffer :wk "Kill this buffer")
    "b n" '(next-buffer :wk "Next buffer")
    "b p" '(previous-buffer :wk "Previous buffer")
    "b r" '(revert-buffer :wk "Reload buffer")
    "b o" '(helm-occur :wk "Occur in buffer")
    "b s" '(save-buffer :wk "Save buffer"))

  (nghia/leader-keys
    "e" '(:ignore t :wk "Edit")
    "e e" '(nghia/edit-emacs-config :wk "Edit Emacs config"))

  (nghia/leader-keys
    "c" '(:ignore t :wk "Code")
    "c s" '(helm-semantic-or-imenu :wk "Semantic symbols")
    "c c" '(comment-line :wk "Comment lines"))

  (nghia/leader-keys
    "p" '(:ignore t :wk "Projectile")
    "p p" '(helm-projectile-switch-project :wk "Project switch")
    "p f" '(helm-projectile-find-file :wk "Project find file")
    "p d" '(helm-projectile-find-dir :wk "Project find dir")
    "p o" '(helm-projectile-find-other-file :wk "Project find other file")
    "p s" '(helm-projectile-ag :wk "Project search")
	"p b" '(helm-projectile-switch-to-buffer :wk "Project change to buffer")
	"p c" '(projectile-compile-project :wk "Project combine"))

  (nghia/leader-keys
    "t" '(:ignore t :wk "Toggle")
    "t l" '(display-line-numbers-mode :wk "Toggle line numbers")
	"t h" '(helm-themes :wk "Toggle themes")
	"t n" '(neotree-toggle :wk "Toggle neotree file viewer")
	"t t" '(toggle-truncate-lines :wk "Toggle truncated lines"))

  (nghia/leader-keys
    "w" '(:ignore t :wk "Windows")
    ;; Window splits
    "w c" '(evil-window-delete :wk "Close window")
    "w n" '(evil-window-new :wk "New window")
    "w s" '(evil-window-split :wk "Horizontal split window")
    "w v" '(evil-window-vsplit :wk "Vertical split window")
    ;; Window motions
    "w h" '(evil-window-left :wk "Window left")
    "w j" '(evil-window-down :wk "Window down")
    "w k" '(evil-window-up :wk "Window up")
    "w l" '(evil-window-right :wk "Window right")
    "w w" '(evil-window-next :wk "Goto next window")
    ;; Move Windows
    "w H" '(nghia/buf-move-left :wk "Buffer move left")
    "w J" '(nghia/buf-move-down :wk "Buffer move down")
    "w K" '(nghia/buf-move-up :wk "Buffer move up")
    "w L" '(nghia/buf-move-right :wk "Buffer move right"))
  )

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key [escape] 'keyboard-escape-quit)
