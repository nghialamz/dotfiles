;;; config-base.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Base setup for emacs startup

;; Profile emacs startup
;; ---
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; General settings
;; ---

;; Default to 4 visible spaces to display a tab
(setq-default tab-width 4)

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)

(setq create-lockfiles nil)
(setq make-backup-files nil) ; stop creating ~ files

;; Set ignore directory
(setq emacs-persistence-directory (concat user-emacs-directory "_ignore/"))
(unless (file-exists-p emacs-persistence-directory)
    (make-directory emacs-persistence-directory t))
(setq ido-save-directory-list-file (concat emacs-persistence-directory "ido-last"))
(setq recentf-save-file (expand-file-name "recentf" emacs-persistence-directory))
(setq projectile-cache-file (expand-file-name "projectile.cach" emacs-persistence-directory))
(setq auto-save-list-file-prefix (expand-file-name "emacs-saves/" emacs-persistence-directory)
	  auto-save-file-name-transforms `((".*" "~/.emacs.d/_ignore/emacs-saves/" t)))
(setq projectile-known-projects-file (expand-file-name "projectile-bookmarks.eld" emacs-persistence-directory)
	  lsp-session-file (expand-file-name ".lsp-session-v1" emacs-persistence-directory))

;; Elpaca - Package manager
;; ---

(defvar elpaca-installer-version 0.7)
(defvar elpaca-directory (expand-file-name "elpaca/" "~/.emacs.d/_ignore/"))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                 ,@(when-let ((depth (plist-get order :depth)))
                                                     (list (format "--depth=%d" depth) "--no-single-branch"))
                                                 ,(plist-get order :repo) ,repo))))
                 ((zerop (call-process "git" nil buffer t "checkout"
                                       (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))
(elpaca-no-symlink-mode)

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable use-package :ensure support for Elpaca.
	(elpaca-use-package-mode)
	(setq elpaca-use-package-by-default t))
