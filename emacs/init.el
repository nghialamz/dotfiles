;;; init.el - Entry point for emacs' configuration
;;; ---
;;; Author: Nghia Lam

(load-file "~/.emacs.d/config-base.el")
(load-file "~/.emacs.d/config-helm.el")
(load-file "~/.emacs.d/config-code.el")
(load-file "~/.emacs.d/config-ui.el")
(load-file "~/.emacs.d/config-org.el")
(load-file "~/.emacs.d/config-utils.el")
(load-file "~/.emacs.d/config-key.el")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-minibuffer-history-key "M-p")
 '(package-selected-packages '(helm-rg)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
