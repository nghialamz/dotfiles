;;; config-ui.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Change Emacs' UI look and feel. Mostly aim for an elegant looking Emacs.

;; UI base setup
;; --
(customize-set-variable 'scroll-bar-mode nil)
(customize-set-variable 'horizontal-scroll-bar-mode nil)
(setq scroll-margin 8)
(setq hscroll-margin 4)
(setq scroll-step 1)
(setq auto-window-vscroll nil)
(setq scroll-up-aggressively 0)
(setq scroll-down-aggressively 0)
(setq visible-bell t)
(setq ring-bell-function 'ignore)
(setq-default line-spacing 0)  ; Line spacing, can be 0 for code and 1 or 2 for text

(setq-default inhibit-startup-screen t)
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(setq initial-scratch-message "Hi Nghia, Let's do better today!")

;; Cursor
;; --
(blink-cursor-mode 0)
(setq global-hl-line-sticky-flag nil)
(global-hl-line-mode)

;; Gaps settings - only set interal-border-width if you want a zen mode like UI. 
;; --
(fringe-mode '(0 . 0))
(add-to-list 'default-frame-alist '(internal-border-width . 0))

;; Icons
;; --

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

(use-package all-the-icons-dired
  :hook (dired-mode . (lambda () (all-the-icons-dired-mode t))))

;; Tree
;; --

(use-package neotree
  :config
  (setq neo-smart-open t
        neo-show-hidden-files t
        neo-window-width 40
        neo-window-fixed-size nil
		inhibit-compacting-font-caches t) 
  (setq neo-theme (if (display-graphic-p) 'icons 'ascii))
  (setq projectile-switch-project-action 'neotree-projectile-action)
  ;; truncate long file names in neotree
  (add-hook 'neo-after-create-hook
			#'(lambda (_)
				(with-current-buffer (get-buffer neo-buffer-name)
				  (setq truncate-lines t)
				  (setq word-wrap nil)
				  (make-local-variable 'auto-hscroll-mode)
				  (setq auto-hscroll-mode nil)))))

;; Themes
;; --
(use-package color-theme-modern)
(use-package naysayer-theme)
(use-package anti-zenburn-theme)
(use-package darktooth-theme)
(use-package zenburn-theme
  :ensure t
  :config
  (load-theme 'zenburn t))

;; Modeline & Headline setup
;; --
(require 'org) ; Change some org face settings

(defun mode-line-render (left right)
  "Return a string of `window-width' length containing left, and
   right aligned respectively."
  (let* ((available-width (- (window-total-width) (length left) )))
    (format (format "%%s %%%ds" available-width) left right)))

(defun apply-modeline-theme (&rest _)
  (setq-default header-line-format
				'(:eval (mode-line-render
						 (format-mode-line
						  (list
						   (propertize " File " 'face `(:weight regular))
						   "%b "
						   '(:eval (if (and buffer-file-name (buffer-modified-p))
									   (propertize "(modified)" 
												   'face `(:weight light
																   :foreground (face-attribute 'default :foreground)))))))
						 (format-mode-line
						  (propertize "%3l:%2c  "
									  'face `(:weight light :foreground (face-attribute 'default :foreground)))))))

  (set-face-attribute 'header-line nil
					  :weight 'bold
					  :height 100
					  :overline nil
                      :underline nil 
                      :foreground (face-attribute 'default :foreground)
					  :background (face-attribute 'default :background)
                      :box `(:line-width 1 :color (face-attribute 'default :foreground) ))
  (set-face-attribute 'mode-line nil
                      :height 5
                      :overline nil
                      :underline (face-attribute 'default :foreground)
                      :background (face-attribute 'default :background)
					  :foreground (face-attribute 'default :background)
                      :box nil)
  (set-face-attribute 'mode-line-inactive nil
                      :box nil
                      :overline nil
                      :underline (face-attribute 'default :foreground)
                      :background (face-attribute 'default :background)
					  :foreground (face-attribute 'default :background)
                      :inherit 'mode-line)
  (set-face-attribute 'mode-line-buffer-id nil 
                      :weight 'light)
  )
(advice-add #'load-theme :after #'apply-modeline-theme)
