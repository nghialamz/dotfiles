;;; config-code.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Configure Emacs for better coding experiences.

;; General settings for coding
;; ---

(delete-selection-mode 1)    ;; You can select text and delete it by typing.
(electric-pair-mode 1)       ;; Turns on automatic parens pairing
(global-auto-revert-mode t)  ;; Automatically show changes if the file has changed

;; Better jump
;; ---
(use-package dumb-jump
  :ensure t
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate)
  (setq xref-show-definitions-function #'xref-show-definitions-completing-read)
  (setq dumb-jump-force-searcher 'ag))

;; Auto-complete
;; ---
(use-package company
  :defer 2
  :diminish
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  (company-show-numbers t)
  (company-tooltip-align-annotations 't)
  (global-company-mode t))

;; Project manager
;; ---
(use-package xref)
(use-package project
  :after xref)

(use-package projectile
  :config
  (setq projectile-track-known-projects-automatically nil)
  (projectile-mode 1))
(use-package helm-projectile
  :after projectile
  :ensure t)

;; Workspace manager
;; ---

(use-package perspective
  :custom
  ;; NOTE! I have also set 'SCP TAB' to open the perspective menu.
  ;; I'm only setting the additional binding because setting it
  ;; helps suppress an annoying warning message.
  (persp-mode-prefix-key (kbd "C-c M-p"))
  :init 
  (persp-mode)
  :config
  ;; Sets a file to write to when we save states
  (setq persp-state-default-file "~/.emacs.d/_ignore/sessions"))

;; This will group buffers by persp-name in ibuffer.
(add-hook 'ibuffer-hook
          (lambda ()
            (persp-ibuffer-set-filter-groups)
            (unless (eq ibuffer-sorting-mode 'alphabetic)
              (ibuffer-do-sort-by-alphabetic))))

;; Automatically save perspective states to file when Emacs exits.
(add-hook 'kill-emacs-hook #'persp-state-save)
