;;; config-helm.el -*- lexical-binding: t; -*-
;;; ---
;;; Author: Nghia Lam
;;;
;;; Configure Helm for the heavy lifting task in Emacs

(use-package helm
  :config
  (setq helm-split-window-in-side-p       t ; open helm buffer inside current window, not occupy whole other window
	helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
	helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
	helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
	helm-ff-file-name-history-use-recentf t
	helm-semantic-fuzzy-match             t
	helm-imenu-fuzzy-match                t
	helm-echo-input-in-header-line        t)
  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)
  (helm-mode 1))

(use-package helm-ag
  :after helm
  :ensure t)

(use-package helm-themes
  :after helm)

(use-package helm-icons
  :after helm
  :config
  (setq helm-icons-provider 'all-the-icons)
  (helm-icons-enable))
